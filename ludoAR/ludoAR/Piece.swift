//
//  File.swift
//  ludoAR
//
//  Created by Clément Robert on 30/01/2019.
//  Copyright © 2019 Clément Robert. All rights reserved.
//

import UIKit

class Piece {
    var topRadius: CGFloat
    var bottomRadius: CGFloat
    var height: CGFloat
    var color: UIColor
    
    init(topRadius: CGFloat, bottomRadius: CGFloat, height: CGFloat, color: UIColor) {
        self.topRadius = topRadius
        self.bottomRadius = bottomRadius
        self.height = height
        self.color = color
    }
}
