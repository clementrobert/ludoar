//
//  ViewController.swift
//  ludoAR
//
//  Created by Clément Robert on 30/01/2019.
//  Copyright © 2019 Clément Robert. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

class ViewController: UIViewController, ARSCNViewDelegate {

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet var sceneView: ARSCNView!
    @IBOutlet weak var msg: UILabel!
    @IBOutlet weak var redButton: UIButton!
    @IBOutlet weak var yellowButton: UIButton!
    @IBOutlet weak var greenButton: UIButton!
    @IBOutlet weak var blueButton: UIButton!
    @IBOutlet weak var colorValidationButton: UIButton!
    var isBoardDisplayed: Bool = false
    var isColorValidated: Bool = false
    var boardNode = SCNNode()
    var redStableNode = SCNNode()
    var yellowStableNode = SCNNode()
    var greenStableNode = SCNNode()
    var blueStableNode = SCNNode()
    var chosenColor: UIColor? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sceneView.delegate = self
        sceneView.showsStatistics = true
        let scene = SCNScene()
        sceneView.scene = scene
        self.msg.text = "Move your phone to detect a flat surface."
        self.msg.textColor = UIColor.yellow
        self.redButton.isHidden = true
        self.yellowButton.isHidden = true
        self.greenButton.isHidden = true
        self.blueButton.isHidden = true
        self.colorValidationButton.isHidden = true
        self.backButton.isHidden = true
        tapToPlaceBoard()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = .horizontal
        sceneView.session.run(configuration)
        sceneView.showsStatistics = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        sceneView.session.pause()
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        guard let planeAnchor = anchor as? ARPlaneAnchor else { return }
        
        let width = CGFloat(planeAnchor.extent.x)
        let height = CGFloat(planeAnchor.extent.z)
        let plane = SCNPlane(width: width, height: height)
        plane.firstMaterial?.diffuse.contents = UIColor.white.withAlphaComponent(0)
        
        let planeNode = SCNNode(geometry: plane)
        
        let x = CGFloat(planeAnchor.center.x)
        let y = CGFloat(planeAnchor.center.y)
        let z = CGFloat(planeAnchor.center.z)
        planeNode.position = SCNVector3(x,y,z)
        planeNode.eulerAngles.x = -.pi / 2
        DispatchQueue.main.async {
            self.msg.text = "Flat surface detected. Tap to place the board."
        }
        
        node.addChildNode(planeNode)
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        guard let planeAnchor = anchor as?  ARPlaneAnchor,
            let planeNode = node.childNodes.first,
            let plane = planeNode.geometry as? SCNPlane
            else { return }
        
        let width = CGFloat(planeAnchor.extent.x)
        let height = CGFloat(planeAnchor.extent.z)
        plane.width = width
        plane.height = height
        
        let x = CGFloat(planeAnchor.center.x)
        let y = CGFloat(planeAnchor.center.y)
        let z = CGFloat(planeAnchor.center.z)
        planeNode.position = SCNVector3(x, y, z)
    }
    
    func tapToPlaceBoard() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ViewController.placeBoard(withGestureRecognizer:)))
        sceneView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func placeBoard(withGestureRecognizer recognizer: UIGestureRecognizer) {
        let tapLocation = recognizer.location(in: sceneView)
        let hitTestResults = sceneView.hitTest(tapLocation, types: .existingPlaneUsingExtent)
        
        guard let hitTestResult = hitTestResults.first else { return }
        let translation = hitTestResult.worldTransform.translation
        let x = translation.x
        let y = translation.y
        let z = translation.z
        
        if !isBoardDisplayed {
            let board = SCNPlane(width: 0.3, height: 0.3)
            board.firstMaterial?.diffuse.contents = UIImage(named: "wood.jpg")
            boardNode.geometry = board
            boardNode.eulerAngles.x = -.pi / 2
            boardNode.position = SCNVector3(x, y, z)
            
            sceneView.scene.rootNode.addChildNode(boardNode)
            let light = SCNLight()
            light.type = .omni
            sceneView.scene.rootNode.light = light
            let redPiece = placePiece(node: boardNode, color: .red, position: SCNVector3(0.14, -0.05, 0.015))
            let yellowPiece = placePiece(node: boardNode, color: .yellow, position: SCNVector3(-0.05, -0.14, 0.015))
            let greenPiece = placePiece(node: boardNode, color: .green, position: SCNVector3(-0.14, 0.05, 0.015))
            let bluePiece = placePiece(node: boardNode, color: .blue, position: SCNVector3(0.05, 0.14, 0.015))
            let dice = placeDice(node: boardNode)
            placeColoredStables(node: boardNode)
            placeColoredDots(node: boardNode)
            placeColoredFinalSquares(node: boardNode)
            startChoosingColor()
            isBoardDisplayed = true
        }
    }
    
    @IBAction func goBack(_ sender: UIButton) {
        chosenColor = nil
        startChoosingColor()
    }
    
    func startChoosingColor() {
        msg.text = "Tap the colored buttons to choose your color."
        self.redButton.isHidden = false
        self.yellowButton.isHidden = false
        self.greenButton.isHidden = false
        self.blueButton.isHidden = false
        self.colorValidationButton.isHidden = false
        self.backButton.isHidden = true
    }
    
    @IBAction func chooseRedColor(_ sender: UIButton) {
        yellowStableNode.removeAllActions()
        yellowStableNode.geometry?.firstMaterial?.diffuse.contents = UIColor.yellow
        greenStableNode.removeAllActions()
        greenStableNode.geometry?.firstMaterial?.diffuse.contents = UIColor.green
        blueStableNode.removeAllActions()
        blueStableNode.geometry?.firstMaterial?.diffuse.contents = UIColor.blue
        chosenColor = .red
        let changeColor = SCNAction.customAction(duration: 1) { (node, elapsedTime) -> () in
            let percentage = elapsedTime / 5
            let color = UIColor(red: 1 - percentage, green: 1 - percentage, blue: 1 - percentage, alpha: 1)
            node.geometry!.firstMaterial!.diffuse.contents = color
        }
        redStableNode.runAction(.repeatForever(changeColor))
    }
    
    @IBAction func chooseYellowColor(_ sender: UIButton) {
        greenStableNode.removeAllActions()
        greenStableNode.geometry?.firstMaterial?.diffuse.contents = UIColor.green
        redStableNode.removeAllActions()
        redStableNode.geometry?.firstMaterial?.diffuse.contents = UIColor.red
        blueStableNode.removeAllActions()
        blueStableNode.geometry?.firstMaterial?.diffuse.contents = UIColor.blue
        chosenColor = .yellow
        let changeColor = SCNAction.customAction(duration: 1) { (node, elapsedTime) -> () in
            let percentage = elapsedTime / 5
            let color = UIColor(red: 1 - percentage, green: 1 - percentage, blue: 1 - percentage, alpha: 1)
            node.geometry!.firstMaterial!.diffuse.contents = color
        }
        yellowStableNode.runAction(.repeatForever(changeColor))
    }
    
    @IBAction func chooseGreenColor(_ sender: UIButton) {
        yellowStableNode.removeAllActions()
        yellowStableNode.geometry?.firstMaterial?.diffuse.contents = UIColor.yellow
        redStableNode.removeAllActions()
        redStableNode.geometry?.firstMaterial?.diffuse.contents = UIColor.red
        blueStableNode.removeAllActions()
        blueStableNode.geometry?.firstMaterial?.diffuse.contents = UIColor.blue
        chosenColor = .green
        let changeColor = SCNAction.customAction(duration: 1) { (node, elapsedTime) -> () in
            let percentage = elapsedTime / 5
            let color = UIColor(red: 1 - percentage, green: 1 - percentage, blue: 1 - percentage, alpha: 1)
            node.geometry!.firstMaterial!.diffuse.contents = color
        }
        greenStableNode.runAction(.repeatForever(changeColor))
    }
    
    @IBAction func chooseBlueColor(_ sender: UIButton) {
        yellowStableNode.removeAllActions()
        yellowStableNode.geometry?.firstMaterial?.diffuse.contents = UIColor.yellow
        redStableNode.removeAllActions()
        redStableNode.geometry?.firstMaterial?.diffuse.contents = UIColor.red
        greenStableNode.removeAllActions()
        greenStableNode.geometry?.firstMaterial?.diffuse.contents = UIColor.green
        chosenColor = .blue
        let changeColor = SCNAction.customAction(duration: 1) { (node, elapsedTime) -> () in
            let percentage = elapsedTime / 5
            let color = UIColor(red: 1 - percentage, green: 1 - percentage, blue: 1 - percentage, alpha: 1)
            node.geometry!.firstMaterial!.diffuse.contents = color
        }
        blueStableNode.runAction(.repeatForever(changeColor))
    }
    
    func printChosenColor() -> String {
        var str: String = ""
        
        switch chosenColor {
        case UIColor.red:
            str = "Red color chosen."
        case UIColor.yellow:
            str = "Yellow color chosen."
        case UIColor.green:
            str = "Green color chosen."
        default:
            str = "Blue color chosen."
        }
        return str
    }
    
    @IBAction func validateColor(_ sender: UIButton) {
        if (chosenColor != nil) {
            redStableNode.removeAllActions()
            redStableNode.geometry?.firstMaterial?.diffuse.contents = UIColor.red
            yellowStableNode.removeAllActions()
            yellowStableNode.geometry?.firstMaterial?.diffuse.contents = UIColor.yellow
            greenStableNode.removeAllActions()
            greenStableNode.geometry?.firstMaterial?.diffuse.contents = UIColor.green
            blueStableNode.removeAllActions()
            blueStableNode.geometry?.firstMaterial?.diffuse.contents = UIColor.blue
            msg.text = printChosenColor()
            redButton.isHidden = true
            yellowButton.isHidden = true
            greenButton.isHidden = true
            blueButton.isHidden = true
            colorValidationButton.isHidden = true
            backButton.isHidden = false
            isColorValidated = true
        }
    }
    
    func placePiece(node: SCNNode, color: UIColor, position: SCNVector3) -> Piece {
        let piece = Piece(topRadius: 0.001, bottomRadius: 0.007, height: 0.03, color: color)
        let pieceCone = SCNCone(topRadius: piece.topRadius, bottomRadius: piece.bottomRadius, height: piece.height)
        pieceCone.firstMaterial?.diffuse.contents = piece.color
        
        let pieceNode = SCNNode(geometry: pieceCone)
        pieceNode.position = position
        pieceNode.eulerAngles.x = .pi / 2
        
        let pieceTop = SCNSphere(radius: 0.005)
        pieceTop.firstMaterial?.diffuse.contents = piece.color
        
        let pieceTopNode = SCNNode(geometry: pieceTop)
        pieceTopNode.position.y = 0.01
        
        pieceNode.addChildNode(pieceTopNode)
        node.addChildNode(pieceNode)
        
        return piece
    }
    
    func placeDice(node: SCNNode) -> SCNNode {
        let diceNode = SCNScene(named: "art.scnassets/dice.scn")!.rootNode
        diceNode.position.z = 0.01
        node.addChildNode(diceNode)
        
        return diceNode
    }
    
    func placeColoredStables(node: SCNNode) {
        let redStable = SCNPlane(width: 0.12, height: 0.12)
        redStable.firstMaterial?.diffuse.contents = UIColor.red
        let yellowStable = SCNPlane(width: 0.12, height: 0.12)
        yellowStable.firstMaterial?.diffuse.contents = UIColor.yellow
        let greenStable = SCNPlane(width: 0.12, height: 0.12)
        greenStable.firstMaterial?.diffuse.contents = UIColor.green
        let blueStable = SCNPlane(width: 0.12, height: 0.12)
        blueStable.firstMaterial?.diffuse.contents = UIColor.blue
        
        redStableNode = SCNNode(geometry: redStable)
        redStableNode.position = SCNVector3(0.0905, -0.0905, 0.001)
        yellowStableNode = SCNNode(geometry: yellowStable)
        yellowStableNode.position = SCNVector3(-0.0905, -0.0905, 0.001)
        greenStableNode = SCNNode(geometry: greenStable)
        greenStableNode.position = SCNVector3(-0.0905, 0.0905, 0.001)
        blueStableNode = SCNNode(geometry: blueStable)
        blueStableNode.position = SCNVector3(0.0905, 0.0905, 0.001)
        
        node.addChildNode(redStableNode)
        node.addChildNode(yellowStableNode)
        node.addChildNode(greenStableNode)
        node.addChildNode(blueStableNode)
    }
    
    func placeColoredDots(node: SCNNode) {
        placeRedDots(node: node)
        placeYellowDots(node: node)
        placeGreenDots(node: node)
        placeBlueDots(node: node)
    }
    
    func placeDot(node: SCNNode, color: UIColor, position: SCNVector3) {
        let dot = SCNCylinder(radius: 0.007, height: 0.0005)
        dot.firstMaterial?.diffuse.contents = color
        
        let dotNode = SCNNode(geometry: dot)
        dotNode.eulerAngles.x = -.pi / 2
        dotNode.position = position
        
        node.addChildNode(dotNode)
    }
    
    func placeRedDots(node: SCNNode) {
        placeDot(node: node, color: .red, position: SCNVector3(0.14, 0, 0.001))
        placeDot(node: node, color: .red, position: SCNVector3(0.14, -0.02, 0.001))
        placeDot(node: node, color: .red, position: SCNVector3(0.12, -0.02, 0.001))
        placeDot(node: node, color: .red, position: SCNVector3(0.10, -0.02, 0.001))
        placeDot(node: node, color: .red, position: SCNVector3(0.08, -0.02, 0.001))
        placeDot(node: node, color: .red, position: SCNVector3(0.06, -0.02, 0.001))
        placeDot(node: node, color: .red, position: SCNVector3(0.04, -0.02, 0.001))
        placeDot(node: node, color: .red, position: SCNVector3(0.02, -0.02, 0.001))
        placeDot(node: node, color: .red, position: SCNVector3(0.02, -0.04, 0.001))
        placeDot(node: node, color: .red, position: SCNVector3(0.02, -0.06, 0.001))
        placeDot(node: node, color: .red, position: SCNVector3(0.02, -0.08, 0.001))
        placeDot(node: node, color: .red, position: SCNVector3(0.02, -0.1, 0.001))
        placeDot(node: node, color: .red, position: SCNVector3(0.02, -0.12, 0.001))
        placeDot(node: node, color: .red, position: SCNVector3(0.02, -0.14, 0.001))
    }
    
    func placeYellowDots(node: SCNNode) {
        placeDot(node: node, color: .yellow, position: SCNVector3(0, -0.14, 0.001))
        placeDot(node: node, color: .yellow, position: SCNVector3(-0.02, -0.14, 0.001))
        placeDot(node: node, color: .yellow, position: SCNVector3(-0.02, -0.12, 0.001))
        placeDot(node: node, color: .yellow, position: SCNVector3(-0.02, -0.10, 0.001))
        placeDot(node: node, color: .yellow, position: SCNVector3(-0.02, -0.08, 0.001))
        placeDot(node: node, color: .yellow, position: SCNVector3(-0.02, -0.06, 0.001))
        placeDot(node: node, color: .yellow, position: SCNVector3(-0.02, -0.04, 0.001))
        placeDot(node: node, color: .yellow, position: SCNVector3(-0.02, -0.02, 0.001))
        placeDot(node: node, color: .yellow, position: SCNVector3(-0.04, -0.02, 0.001))
        placeDot(node: node, color: .yellow, position: SCNVector3(-0.06, -0.02, 0.001))
        placeDot(node: node, color: .yellow, position: SCNVector3(-0.08, -0.02, 0.001))
        placeDot(node: node, color: .yellow, position: SCNVector3(-0.1, -0.02, 0.001))
        placeDot(node: node, color: .yellow, position: SCNVector3(-0.12, -0.02, 0.001))
        placeDot(node: node, color: .yellow, position: SCNVector3(-0.14, -0.02, 0.001))
    }
    
    func placeGreenDots(node: SCNNode) {
        placeDot(node: node, color: .green, position: SCNVector3(-0.14, 0, 0.001))
        placeDot(node: node, color: .green, position: SCNVector3(-0.14, 0.02, 0.001))
        placeDot(node: node, color: .green, position: SCNVector3(-0.12, 0.02, 0.001))
        placeDot(node: node, color: .green, position: SCNVector3(-0.10, 0.02, 0.001))
        placeDot(node: node, color: .green, position: SCNVector3(-0.08, 0.02, 0.001))
        placeDot(node: node, color: .green, position: SCNVector3(-0.06, 0.02, 0.001))
        placeDot(node: node, color: .green, position: SCNVector3(-0.04, 0.02, 0.001))
        placeDot(node: node, color: .green, position: SCNVector3(-0.02, 0.02, 0.001))
        placeDot(node: node, color: .green, position: SCNVector3(-0.02, 0.04, 0.001))
        placeDot(node: node, color: .green, position: SCNVector3(-0.02, 0.06, 0.001))
        placeDot(node: node, color: .green, position: SCNVector3(-0.02, 0.08, 0.001))
        placeDot(node: node, color: .green, position: SCNVector3(-0.02, 0.1, 0.001))
        placeDot(node: node, color: .green, position: SCNVector3(-0.02, 0.12, 0.001))
        placeDot(node: node, color: .green, position: SCNVector3(-0.02, 0.14, 0.001))
    }
    
    func placeBlueDots(node: SCNNode) {
        placeDot(node: node, color: .blue, position: SCNVector3(0, 0.14, 0.001))
        placeDot(node: node, color: .blue, position: SCNVector3(0.02, 0.14, 0.001))
        placeDot(node: node, color: .blue, position: SCNVector3(0.02, 0.12, 0.001))
        placeDot(node: node, color: .blue, position: SCNVector3(0.02, 0.10, 0.001))
        placeDot(node: node, color: .blue, position: SCNVector3(0.02, 0.08, 0.001))
        placeDot(node: node, color: .blue, position: SCNVector3(0.02, 0.06, 0.001))
        placeDot(node: node, color: .blue, position: SCNVector3(0.02, 0.04, 0.001))
        placeDot(node: node, color: .blue, position: SCNVector3(0.02, 0.02, 0.001))
        placeDot(node: node, color: .blue, position: SCNVector3(0.04, 0.02, 0.001))
        placeDot(node: node, color: .blue, position: SCNVector3(0.06, 0.02, 0.001))
        placeDot(node: node, color: .blue, position: SCNVector3(0.08, 0.02, 0.001))
        placeDot(node: node, color: .blue, position: SCNVector3(0.1, 0.02, 0.001))
        placeDot(node: node, color: .blue, position: SCNVector3(0.12, 0.02, 0.001))
        placeDot(node: node, color: .blue, position: SCNVector3(0.14, 0.02, 0.001))
    }
    
    func placeFinalSquare(node: SCNNode, color: UIColor, plane: SCNPlane, position: SCNVector3) {
        let finalSquare = plane
        finalSquare.firstMaterial?.diffuse.contents = color
        
        let finalSquareNode = SCNNode(geometry: finalSquare)
        finalSquareNode.position = position
        
        node.addChildNode(finalSquareNode)
    }
    
    func placeColoredFinalSquares(node: SCNNode) {
        placeRedFinalSquares(node: node)
        placeYellowFinalSquares(node: node)
        placeGreenFinalSquares(node: node)
        placeBlueFinalSquares(node: node)
    }
    
    func placeRedFinalSquares(node: SCNNode) {
        placeFinalSquare(node: node, color: .red, plane: SCNPlane(width: 0.01, height: 0.015), position: SCNVector3(0.12, 0, 0.001))
        placeFinalSquare(node: node, color: .red, plane: SCNPlane(width: 0.01, height: 0.015), position: SCNVector3(0.1, 0, 0.001))
        placeFinalSquare(node: node, color: .red, plane: SCNPlane(width: 0.01, height: 0.015), position: SCNVector3(0.08, 0, 0.001))
        placeFinalSquare(node: node, color: .red, plane: SCNPlane(width: 0.01, height: 0.015), position: SCNVector3(0.06, 0, 0.001))
        placeFinalSquare(node: node, color: .red, plane: SCNPlane(width: 0.01, height: 0.015), position: SCNVector3(0.04, 0, 0.001))
        placeFinalSquare(node: node, color: .red, plane: SCNPlane(width: 0.01, height: 0.015), position: SCNVector3(0.02, 0, 0.001))
    }
    
    func placeYellowFinalSquares(node: SCNNode) {
        placeFinalSquare(node: node, color: .yellow, plane: SCNPlane(width: 0.015, height: 0.01), position: SCNVector3(0, -0.12, 0.001))
        placeFinalSquare(node: node, color: .yellow, plane: SCNPlane(width: 0.015, height: 0.01), position: SCNVector3(0, -0.1, 0.001))
        placeFinalSquare(node: node, color: .yellow, plane: SCNPlane(width: 0.015, height: 0.01), position: SCNVector3(0, -0.08, 0.001))
        placeFinalSquare(node: node, color: .yellow, plane: SCNPlane(width: 0.015, height: 0.01), position: SCNVector3(0, -0.06, 0.001))
        placeFinalSquare(node: node, color: .yellow, plane: SCNPlane(width: 0.015, height: 0.01), position: SCNVector3(0, -0.04, 0.001))
        placeFinalSquare(node: node, color: .yellow, plane: SCNPlane(width: 0.015, height: 0.01), position: SCNVector3(0, -0.02, 0.001))
    }
    
    func placeGreenFinalSquares(node: SCNNode) {
        placeFinalSquare(node: node, color: .green, plane: SCNPlane(width: 0.01, height: 0.015), position: SCNVector3(-0.12, 0, 0.001))
        placeFinalSquare(node: node, color: .green, plane: SCNPlane(width: 0.01, height: 0.015), position: SCNVector3(-0.1, 0, 0.001))
        placeFinalSquare(node: node, color: .green, plane: SCNPlane(width: 0.01, height: 0.015), position: SCNVector3(-0.08, 0, 0.001))
        placeFinalSquare(node: node, color: .green, plane: SCNPlane(width: 0.01, height: 0.015), position: SCNVector3(-0.06, 0, 0.001))
        placeFinalSquare(node: node, color: .green, plane: SCNPlane(width: 0.01, height: 0.015), position: SCNVector3(-0.04, 0, 0.001))
        placeFinalSquare(node: node, color: .green, plane: SCNPlane(width: 0.01, height: 0.015), position: SCNVector3(-0.02, 0, 0.001))
    }
    
    func placeBlueFinalSquares(node: SCNNode) {
        placeFinalSquare(node: node, color: .blue, plane: SCNPlane(width: 0.015, height: 0.01), position: SCNVector3(0, 0.12, 0.001))
        placeFinalSquare(node: node, color: .blue, plane: SCNPlane(width: 0.015, height: 0.01), position: SCNVector3(0, 0.1, 0.001))
        placeFinalSquare(node: node, color: .blue, plane: SCNPlane(width: 0.015, height: 0.01), position: SCNVector3(0, 0.08, 0.001))
        placeFinalSquare(node: node, color: .blue, plane: SCNPlane(width: 0.015, height: 0.01), position: SCNVector3(0, 0.06, 0.001))
        placeFinalSquare(node: node, color: .blue, plane: SCNPlane(width: 0.015, height: 0.01), position: SCNVector3(0, 0.04, 0.001))
        placeFinalSquare(node: node, color: .blue, plane: SCNPlane(width: 0.015, height: 0.01), position: SCNVector3(0, 0.02, 0.001))
    }
}

extension float4x4 {
    var translation: float3 {
        let translation = self.columns.3
        return float3(translation.x, translation.y, translation.z)
    }
}
